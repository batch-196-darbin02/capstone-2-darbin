const jwt = require("jsonwebtoken");

const secret = "eCommerceAPI";

module.exports.createAccessToken = (userDetails) => {
	
	const data = {
		id: userDetails.id,
		email: userDetails.email,
		isAdmin: userDetails.isAdmin
	}

	console.log(data);

	return jwt.sign(data,secret,{});

}


module.exports.verify = (req,res,next) => {

	let token = req.headers.authorization

	if(typeof token === "undefined"){
		return res.send({auth: "Failed. No token."});
	
	} else {

		token = token.slice(7);
		
		jwt.verify(token,secret,function(err,decodedToken){

			if(err) {
				return res.send({

					auth: "Failed",
					message: err.message
				})
			
			} else {

				req.user = decodedToken;

				next();
			}
		})
	}
};


module.exports.verifyAdmin = (req,res,next) => {

	console.log(req.user);

	if(req.user.isAdmin) {

		next();

	} else {

		return res.send({

			auth: "Failed",
			message: "Action Forbidden"
		})
	}
};