const express = require("express");

const router = express.Router();

const userControllers = require("../controllers/userControllers");

const auth = require("../auth");

const {verify,verifyAdmin } = auth;


router.post('/',userControllers.registerUser);

router.get('/getUserDetails',verify,userControllers.getUserDetails);

router.post('/login',userControllers.loginUser);

router.put('/setAsAdmin/:id',verify,verifyAdmin,userControllers.setAsAdmin);

router.post('/createOrder',verify,userControllers.createOrder);

router.get('/getUserOrders',verify,userControllers.getUserOrders);

router.get('/allOrders',verify,verifyAdmin,userControllers.getAllOrders);





module.exports = router;