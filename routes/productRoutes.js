const express = require("express");

const router = express.Router();

const productControllers = require("../controllers/productControllers");

const auth = require("../auth");

const { verify,verifyAdmin } = auth;


router.post('/',verify,verifyAdmin,productControllers.AddProduct);

router.get('/activeProducts',productControllers.getActiveProducts);

router.get('/getSingleProduct/:productId',productControllers.getSingleProduct);

router.put('/updateProduct/:productId',verify,verifyAdmin,productControllers.updateProduct);

router.delete('/archiveProduct/:productId',verify,verifyAdmin,productControllers.archiveProduct);









module.exports = router;