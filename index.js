const express = require("express");

const mongoose = require("mongoose");

const app = express();

const port = 4000


mongoose.connect("mongodb+srv://admin:admin123@cluster0.6dj5p.mongodb.net/eCommerceAPI?retryWrites=true&w=majority",
{
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;

db.on(`error`,console.error.bind(console,"MongoDB Connection Error."))

db.once(`open`,() => console.log("Connected to MongoDB."))

app.use(express.json());

/*app.get('/products', (req,res) => {
	res.send("This route will get all products documents.");
});

app.post('/products', (req,res) => {
	res.send("This route will create a new product document.");
});*/

const productRoutes = require('./routes/productRoutes');
app.use('/products',productRoutes);

const userRoutes = require('./routes/userRoutes');
app.use('/users',userRoutes);


app.listen(port,()=>console.log(`Server is running at port ${port}`));