const User = require("../models/User");

const Product = require("../models/Product");

const bcrypt = require("bcrypt");

const auth = require("../auth");


module.exports.registerUser = (req,res) => {

	const hashedPw = bcrypt.hashSync(req.body.password,10)
	console.log(hashedPw);

	let newUser = new User ({

		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,
		password: hashedPw,
		mobileNo: req.body.mobileNo

	});

	newUser.save()
	.then(result => res.send(result))
	.catch(error => res.send(error))

};


module.exports.getUserDetails = (req,res) => {
	
	// console.log(req.user);
	User.findById(req.user.id)
	.then(result => res.send(result))
	.catch(error => res.send(error))

};


module.exports.loginUser = (req,res) => {

	console.log(req.body);

	User.findOne({email:req.body.email})
	.then(foundUser => {

		if(foundUser === null){
			return res.send({message: "No user found."});

		} else {

			const isPasswordCorrect = bcrypt.compareSync(req.body.password,foundUser.password);

			if(isPasswordCorrect){
				return res.send({accessToken: auth.createAccessToken(foundUser)});

			} else {

				return res.send({message: "Incorrect password."});
			}
		}
	})
};


module.exports.setAsAdmin = (req,res) => {

	let update = {

		isAdmin: true

	};

	User.findByIdAndUpdate(req.params.id,update,{new:true})
	.then(result => res.send(result))
	.catch(error => res.send(error))

};


module.exports.createOrder = async (req,res) => {

	if(req.user.isAdmin){
		return res.send({message: "Action not allowed."});
	}

	let isUserUpdated = await User.findById(req.user.id).then(user => {

/*		let newOrder = {
			totalAmount: req.body.totalAmount,
			productId: req.body.productId,
			quantity: req.body.quantity
		};*/

		user.orders.push(req.body);

		let lastOrder = user.orders[user.orders.length-1];
		let orderId = lastOrder.id;
		let productArr = req.body.products;
		// console.log(productArr);

		productArr.forEach(function(productDetail){
			console.log(productDetail);

		Product.findById(productDetail.productId).then(product => {

		// console.log(product);

		let buyer = {
			orderId: orderId,
			userId: req.user.id,
			quantity: productDetail.quantity
		};

		product.orders.push(buyer);

		product.save().then().catch(err => err.message);

	});
		})

		return user.save()
		.then(user => true)
		.catch(err => err.message);

	});

	if(isUserUpdated !== true){
		return res.send({message: isUserUpdated});
	} else {
		return res.send({message: "Order is successful"})
	};

};


module.exports.getUserOrders = (req,res) => {
	
	User.findById(req.user.id,{firstName:1,lastName:1,orders:1})
	.then(result => res.send(result))
	.catch(error => res.send(error))
};


module.exports.getAllOrders = (req,res) => {
	
	User.find({isAdmin:false},{firstName:1,lastName:1,orders:1})
	.then(result => res.send(result))
	.catch(error => res.send(error))
};